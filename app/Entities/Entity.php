<?php

namespace App\Entities;

use Gedmo\Timestampable\Traits\Timestampable;

abstract class Entity {

	use Timestampable;

	protected $id;

	public function __construct($attributes = []) {
        array_walk($attributes, function ($value, $key) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        });
    }

    public function getId()
    {
    	return $this->id;
    }

	public function __get($key)
    {
        if (method_exists($this, 'get' . ucfirst($key))) {
            return $this->{'get' . ucfirst($key)}();
        }

        return null;
    }

}
