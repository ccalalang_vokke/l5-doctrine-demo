<?php
/**
 * Created by PhpStorm.
 * User: Get Devs
 * Date: 27/02/2018
 * Time: 12:59 PM
 */

namespace App\Entities;


class Group extends Entity
{
    protected $name;

    protected  $description;

    protected $users;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}