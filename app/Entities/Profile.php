<?php
/**
 * Created by PhpStorm.
 * User: Get Devs
 * Date: 27/02/2018
 * Time: 10:56 AM
 */

namespace App\Entities;


class Profile extends Entity
{
    protected $firstName;

    protected $lastName;

    protected $birthday;

    protected $gender;

    protected $mobileNumber;

    public function getName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumberp;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
    }
}