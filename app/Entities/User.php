<?php

namespace App\Entities;

use LaravelDoctrine\ORM\Auth\Authenticatable;

class User extends Entity
{
    use Authenticatable;

    protected $username;

    protected $email;

    protected $profile;

    protected $article;

    protected $groups;

    protected $address;

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


}