<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20180227224709 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD address_street VARCHAR(255) NOT NULL, ADD address_postal_code VARCHAR(255) NOT NULL, ADD address_city VARCHAR(255) NOT NULL, ADD address_country VARCHAR(255) NOT NULL, ADD address_created_at DATETIME NOT NULL, ADD address_updated_at DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP address_street, DROP address_postal_code, DROP address_city, DROP address_country, DROP address_created_at, DROP address_updated_at');
    }
}
